# haskell-project-setup

## Overview

This README contains instructions on setting up Haskell projects on MacOS.

1. Haskell installation, tooling, project setup, and configuration, using https://lexi-lambda.github.io/blog/2018/02/10/an-opinionated-guide-to-haskell-in-2018/
2. How to configure VSCode on MacOS for working with Haskell projects, based on these articles:
  * https://svejcar.dev/posts/2020/01/01/my-vscode-setup-for-haskell/
  * https://medium.com/@dogwith1eye/setting-up-haskell-in-vs-code-with-stack-and-the-ide-engine-81d49eda3ecf
  * https://dev.to/egregors/vscode-for-haskell-in-2020-5dn8


This is a step-by-step and concise summary of the commands needed to set things up based on those articles.

NOTE: This is just the commands. Read the articles above for details and context.

## Installation and initial configuration of `stack`

* `brew install haskell-stack`
* Edit `~/.stack/config.yaml`
  * Enter name, email, and copyright. See https://docs.haskellstack.org/en/stable/yaml_configuration/#templates

## Creating a new project

* `stack new my-haskell-project -p "category:MyProjectCategory"`
  * Where `MyProjectCategory` is your project's category. See https://docs.haskellstack.org/en/stable/yaml_configuration/#templates and https://hackage.haskell.org/packages/ for category values.

## haskell-linter

* Run from root of project: `stack build --copy-compiler-tool hlint`
* It should now be installed under `~/.stack/compiler-tools/x86_64-osx/ghc-8.8.3/bin/hlint`
* At this point, you should have a [`stack.yaml.lock`](https://docs.haskellstack.org/en/stable/lock_files/). Commit and push it.

## Project docs setup

From project dir:

* `stack build --copy-compiler-tool hoogle`
* ~~`stack hoogle -- generate --local`~~ - this doesn't work!
* `stack hoogle -- generate`
  * Then, to run server: `stack hoogle -- server --local --port=8080`
  * ..and open at http://localhost:8080

## VSCode setup

See [VSCODE_SETUP.md](VSCODE_SETUP.md) - but note this did not work out. Never got it to fully work.

##  IntelliJ setup

See [INTELLIJ_SETUP.md](INTELLIJ_SETUP.md)

## Run ghci

* `stack ghci`
* Exit with `:q` (TODO: other options?)

## Build your project

* `stack test --fast --haddock-deps --copy-bins my-haskell-project`
* Run it (should be in `~/.local/bin`, which should be on yoru path): `my-haskell-project-exe`

