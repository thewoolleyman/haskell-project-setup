## VSCode setup

Note: To manually edit settings files, see info in https://code.visualstudio.com/docs/getstarted/settings#_settings-file-locations, or use the "Open Settings (JSON)" command.

### Haskell Syntax Highlighting

* Install extension: [`Haskell Syntax Highlighting`](https://github.com/JustusAdam/language-haskell)

### haskell-linter

* Install extension: [`haskell-linter`](https://github.com/hoovercj/vscode-haskell-linter)
* (If not done yet) Run from root of project: `stack build --copy-compiler-tool hlint`
* Preferences -> Settings -> Extensions -> haskell-linter -> Haskell > Hlint: Executable Path: `/Users/cwoolley/.stack/compiler-tools/x86_64-osx/ghc-8.8.3/bin/hlint` (use the appropriate path which you just installed there via `--copy-compiler-tool` - it must be absolute, can't use `~`)
* At this point, you should have a [`stack.yaml.lock`](https://docs.haskellstack.org/en/stable/lock_files/). Commit and push it.

### Haskell Language Server

TODO: that contrary to the advice in Lexi's guide, this does a global install. I don't know how to make it install to the local project.  But this [may not be necessary with `hie-wrapper`](https://github.com/haskell/haskell-ide-engine#multiple-versions-of-hie-optional)?

* Add `~/.local/bin` to your `PATH` in `~/.zshrc`
* Check your GHC version from within project dir: `stack ghc -- --version`
* From your workspace (`cd ..`, in a peer directory to your project) run:
  * `git clone https://github.com/haskell/haskell-ide-engine --recurse-submodules`
* `cd haskell-ide-engine`
* `stack ./install.hs hie-8.8.3` - replace `8.8.3` with the `ghc` version from the command above.
* `stack ./install.hs data`
* `stack --stack-yaml=stack-8.8.3.yaml exec hoogle generate` - replace version here too
* Install the extension in VSCode: `Haskell Language Server`
* If needed, set the "Hie executable path" in extension preferences to `~/.local/bin/hie-8.8.3`
* Hover over the `IO` monad in `app/Main.hs`, and you should see information about it.

## Debugging Extension

TODO: document setup from https://medium.com/@dogwith1eye/setting-up-haskell-in-vs-code-with-stack-and-the-ide-engine-81d49eda3ecf
