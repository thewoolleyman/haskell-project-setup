## IntelliJ Setup

### Install Plugin

Install the "IntelliJ-Haskell" plugin - not any other one.

### Install ormolu

* `stack build --copy-compiler-tool ormolu`

### Install stylish-haskell

* `stack build --copy-compiler-tool stylish-haskell`

### Configure Plugin

* Go to Preferences -> Languages & Frameworks -> Haskell
* Check "Build tools using system GHC"
* Check "Use Custom Haskell Tools"
* Enter paths to all tools. They should be under `~/.stack/compiler-tools/x86_64-osx/ghc-8.8.3/bin/TOOL_EXECUTABLE`
* Follow instructions [here](https://github.com/rikvdkleij/intellij-haskell/blob/master/README.md#getting-started)